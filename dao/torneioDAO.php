<?php
	error_reporting(E_ALL);
	ini_set("display_errors", 1);
	require_once "../control/Conexao.php";
	require_once "enderecoDAO.php";
	require_once '../model/torneio.php';
	require_once "TorneioAtletaDAO.php";

	class TorneioDAO{
		private $con;
		function __construct()
		{
			$this->con = Conexao::conectar();
		}
		function inserir(Torneio $t){
			try {
				$daoEndereco=new enderecoDAO();
				$IDEndereco= $daoEndereco->inserir($t->getEndereco());
				$sql = "INSERT INTO `tbtorneio`(`NomeTorneio`, `Descricao`, `DtTorneio`, `ValorInsc`, `IDEndereco`) VALUES ('".$t->getNome()."','".$t->getDesc()."','".$t->getRealizacao()."','".$t->getVpag()."','".$IDEndereco."')";
				$stmt = $this->con-> prepare($sql);
				if ($stmt->execute()) {
					$_SESSION['tornSucess'] = true;
				} else {
					$_SESSION['tornFail'] = true;
				}
				header('Location: ../view/listagemTorn.php');
			}catch(PDOException $e){
			echo 'Error: '.$e->getMessage();
			}
		}
		function alterar(Torneio $t,$id){
			try {
				$daoEndereco=new enderecoDAO();
				$sql = "UPDATE `tbtorneio` SET `NomeTorneio`='".$t->getNome()."', `Descricao`='".$t->getDesc()."',`DtTorneio`='".$t->getRealizacao()."',`ValorInsc`='".$t->getVpag()."' WHERE `IDTorneio`=".$id;
				$stmt = $this->con-> prepare($sql);
				if ($stmt->execute()) {
					$_SESSION['tornASucess'] = true;
				} else {
					$_SESSION['tornAFail'] = true;
				}
				header('Location: ../view/listagemTorn.php');
			}catch(PDOException $e){
			echo 'Error: '.$e->getMessage();
			}
		}
		function listagem($ordem){
			try{
				$sql = "SELECT `IDTorneio`,`NomeTorneio`, `DtTorneio`, `TipoTorneio`, `ValorInsc`, `Situacao` FROM `tbtorneio` ORDER BY `".$ordem."`";
				$stmt = $this->con->prepare($sql);
				$stmt->execute();
				$lista = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $lista;
			}catch(PDOException $e){
			echo 'Error: '.$e->getMessage();
			}
		}
		function listagemID($id){
			try{
				$sql = "SELECT * FROM `tbtorneio` WHERE  `IDTorneio`=".$id."";
				$stmt = $this->con->prepare($sql);
				$stmt->execute();
				$lista = $stmt->fetch(PDO::FETCH_ASSOC);
				//var_dump($sql);
				return $lista;
			}catch(PDOException $e){
			echo 'Error: '.$e->getMessage();
			}
		}
		function consultaClasse($idcat){
			$sql="SELECT * FROM `tbclassecategoria` WHERE IDCategoria =".$idcat;
			$stmt = $this->con->prepare($sql);
			$stmt->execute();
			$classe = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $classe;
		}
		function deletarTorneio($id){
			try {
				$daoInscricao=new TorneioAtletaDAO();
				$daoInscricao->deletarInscricao($id);
				$sql = "DELETE FROM `tbtorneio` WHERE IDTorneio=".$id;
				$stmt = $this->con-> prepare($sql);
				if ($stmt->execute()) {
					$_SESSION['delTornSucess'] = true;
				} else {
					$_SESSION['delTornFail'] = true;
				}
				header('Location: ../view/listagemTorn.php');
			}catch(PDOException $e){
			echo 'Error: '.$e->getMessage();
			}
		}
	}
?>