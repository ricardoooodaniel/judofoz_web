<?php
	require_once '../control/Conexao.php';
	require_once '../model/login.php';

	
	class LoginDAO{
		private $con;
		function __construct(){
			$this->con = Conexao::conectar();
		}
		function verificar(Login $l){
			try{
				$sql = "SELECT * FROM `tbatleta` WHERE `Login`= :login AND `Senha`= md5(:senha)";
				$stm = $this->con->prepare($sql);
				$stm->bindValue(":login",$l->getUsuario(), PDO::PARAM_STR);
				$stm->bindValue(":senha",$l->getSenha(), PDO::PARAM_STR);
				$stm->execute();
				if ($stm->rowCount()==1) {
					$user= $stm->fetch();
					$user['Faixa'] = $this->consultaFaixa($user['IDFaixa']);
					$user['Endereco'] = $this->consultaEndereco($user['IDEndereco']);
					$_SESSION['usuario'] = $user;
					if ($user['TipoAtleta']==1) {
						return 1;
					}elseif ($user['TipoAtleta']==2) {
						return 2;
					}
				}else{
					return 0;
				}
			}catch(PDOException $e){
				echo 'Error: '.$e->getMessage();
			}
		}
		function consultaEndereco($id){
			$sql = "SELECT * FROM `tbendereco` WHERE `IDEndereco`=:id";
			$stm = $this->con->prepare($sql);
			$stm->bindValue(":id",$id, PDO::PARAM_STR);
			$stm->execute();
			return $stm->fetch();
		}
		function consultaFaixa($id){
			$sql ="SELECT * FROM `tbfaixa` WHERE `IDFaixa`=:id";
			$stm = $this->con->prepare($sql);
			$stm->bindValue(":id",$id, PDO::PARAM_STR);
			$stm->execute();
			return $stm->fetch();
		}
	}
?>