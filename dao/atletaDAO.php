<?php
	error_reporting(E_ALL);
	ini_set("display_errors", 1);

	require_once "../control/Conexao.php";
	require_once "enderecoDAO.php";
	require_once '../model/atleta.php';
	require_once 'loginDAO.php';

	class AtletaDAO{
		private $con;
		private $conec;
		function __construct(){
			$this->con = Conexao::conectar();
			$this->conec = Conexao::conectar();
		}
		function inserir(Atleta $a){
			try{
				session_start();
				$log="SELECT * FROM `tbatleta` WHERE Login= :login OR EmailAtleta= :email";
				$stm = $this->con-> prepare($log);
				$stm->bindValue(":login",$a->getUsuario(), PDO::PARAM_STR);
				$stm->bindValue(":email",$a->getEmail(), PDO::PARAM_STR);
				$stm->execute();
				echo $a->getimg();
				$i;
				$b;
				if ($a->getImg()<>null) {
						$i=$a->getImg();
					}else{
						$i='pendente.png';
					}
				if ($stm->rowCount()==0) {
					$_SESSION['cadastrado'] = true;
					$daoEndereco=new enderecoDAO();
					$IDEndereco= $daoEndereco->inserir($a->getEndereco());
					$sql = "INSERT INTO tbatleta(Login, Senha, EmailAtleta, TelefoneAtleta, NomeAtleta, TipoAtleta, IDFaixa, GeneroAtleta, DataNasc, Peso, Altura, IDEndereco, FotoAtleta) VALUES ('".$a->getUsuario()."',md5('".$a->getSenha()."'),'".$a->getEmail()."','".$a->getTelefone()."','".$a->getNome()."', 1 ,'".$a->getFaixa()."','".$a->getGenero()."','".$a->getNascimento()."','".$a->getPeso()."','".$a->getAltura()."','".$IDEndereco."','".$i."')";//:login, md5(:senha), :email, :telefone, :nome, 1, :faixa, :genero, :nasc, :peso, :altura, :endereco)'";
					$stmt = $this->conec-> prepare($sql);
					$stmt->execute();
					header('Location: ../index.php');
				}else{
					$_SESSION['existente'] = true;
					if ($a->getImg()<>null) {
						unlink('../img/Atletas/'.$a->getImg());
					}					
					//header('Location: ../view/atleta.php');
				}
			
			/*$stm->bindValue(":login",$a->getUsuario(), PDO::PARAM_STR);
			$stm->bindValue(":senha",$a->getSenha(), PDO::PARAM_STR);
			$stm->bindValue(":email",$a->getEmail(), PDO::PARAM_STR);
			$stm->bindValue(":telefone",$a->getTelefone(), PDO::PARAM_STR);
			$stm->bindValue(":nome",$a->getNome(), PDO::PARAM_STR);
			$stm->bindValue(":faixa",$a->getFaixa(), PDO::PARAM_INT);
			$stm->bindValue(":genero",$a->getGenero(), PDO::PARAM_INT);
			$stm->bindValue(":nasc",$a->getNascimento(), PDO::PARAM_STR);
			$stm->bindValue(":peso",$a->getPeso(), PDO::PARAM_STR);
			$stm->bindValue(":altura",$a->getAltura(), PDO::PARAM_STR);
			$stm->bindValue(":endereco",$IDEndereco, PDO::PARAM_INT);*/
			} catch(PDOException $e){
			echo 'Error: '.$e->getMessage();
			}
		}
		function consulta($id){
			try{
				$dao = new LoginDAO();
				$sql = "SELECT * FROM `tbatleta` WHERE IDAtleta=".$id;
				$stm = $this->con->prepare($sql);
				if ($stm->execute()){
					$atleta = $stm->fetch(PDO::FETCH_ASSOC);
					$atleta['Faixa'] = $dao->consultaFaixa($atleta['IDFaixa']);
					$atleta['Endereco'] = $dao->consultaEndereco($atleta['IDEndereco']);
					return $atleta;
				}else{
					return false;
				}
			}catch(PDOException $e){
				echo 'Error: '.$e->getMessage();
			}
		}
	}
?>