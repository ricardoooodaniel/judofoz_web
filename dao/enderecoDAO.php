<?php
	/* Informa o nível dos erros que serão exibidos */
	error_reporting(E_ALL);
 
	/* Habilita a exibição de erros */
	ini_set("display_errors", 1);

	require_once "../control/Conexao.php";
	require_once '../model/endereco.php';
	class EnderecoDAO{
		private $con;
		function __construct(){
			$this->con = Conexao::conectar();
		}
		function inserir(endereco $e){
			try{
			//$sql = "INSERT INTO tbendereco('CEP', 'Estado', 'Municipio', 'Bairro', 'Rua', 'NResidencia', 'Complemento') VALUES (:cep, :estado, :municipio, :bairro, :rua, :numero, :complemento)";
			$sql = "INSERT INTO tbendereco (CEP, Estado, Municipio, Bairro, Rua, NResidencia, Complemento) VALUES ('".$e->getCEP()."', '".$e->getEstado()."', '".$e->getCidade()."', '".$e->getBairro()."', '".$e->getRua()."', '".$e->getNumero()."', '".$e->getComplemento()."')";
			//$sql = "INSERT INTO tbendereco (CEP, Estado, Município, Bairro, Rua, NResidência, Complemento) VALUES ('123', '11', 'foz', 'vila', 'chão', '1', 'complemento')";
			//echo $sql;
			/*$stm-> bindValue(":estado",$e->getEstado(), PDO::PARAM_STR);
			$stm-> bindValue(":complemento",$e->getComplemento(), PDO::PARAM_STR);
			$stm-> bindValue(":rua",$e->getRua(), PDO::PARAM_STR);
			$stm-> bindValue(":numero",$e->getNumero(), PDO::PARAM_INT);
			$stm-> bindValue(":bairro",$e->getBairro(), PDO::PARAM_STR);
			$stm-> bindValue(":municipio",$e->getCidade(), PDO::PARAM_STR);
			$stm-> bindValue(":cep",$e->getCEP(), PDO::PARAM_STR);*/
			$this->con-> exec($sql);
			//echo $sql;
			//var_dump($this->con->exec("INSERT INTO tbendereco (CEP, Estado, Municipio, Bairro, Rua, NResidencia, Complemento) VALUES ('123', '11', 'foz', 'vila', 'chão', '1', 'complemento')"));
			return $this->con->lastInsertId();
			} catch(PDOException $e){
				echo 'Error: '.$e->getMessage();
			}
		}
		function buscaPorCEP($cep){
			$sql ="SELECT * FROM `tbendereco` WHERE `CEP` = ".$cep;
			$stm = $this->con->prepare($sql);
			$stm->execute();
			if ($stm->rowCount()<>0) {
				return $stm->fetch();
			} else{
				return null;
			}
		}
	}
?>