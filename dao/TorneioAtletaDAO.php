<?php
	require_once '../control/Conexao.php';
	class TorneioAtletaDAO{	
		private $con;
		function __construct(){
			$this->con = Conexao::conectar();
		}
		function consultarInscricao(Inscricao $i){
			try{
				$sql = "SELECT * FROM `tbinscricao` WHERE `IDAtleta`= ? AND `IDTorneio`= ?";
				$stm = $this->con->prepare($sql);
				$stm->bindValue("1",$i->getIDAtleta(), PDO::PARAM_STR);
				$stm->bindValue("2",$i->getIDTorneio(), PDO::PARAM_STR);
				var_dump($sql);
				$stm->execute();
				if ($stm->rowCount()>=1) {
					return 1;
				}else{
					$e = $this->gerarInscricao($i);
					return $e;
				}
			}catch(PDOException $e){
				echo 'Error: '.$e->getMessage();
			}
		}
		function gerarInscricao(Inscricao $i) {
			try{
				$sql = "INSERT INTO `tbinscricao` (`IDAtleta`, `IDTorneio`, `IDClasseCategoria`) VALUES (?,?,?)";
				$stm = $this->con->prepare($sql);
				$stm->bindValue("1",$i->getIDAtleta(), PDO::PARAM_STR);
				$stm->bindValue("2",$i->getIDTorneio(), PDO::PARAM_STR);
				$stm->bindValue("3",$i->getClasseCategoria(), PDO::PARAM_STR);
				var_dump($stm);
				if ($stm->execute()) {
					return 2;
				} else{
					return 3;
				}
			}catch(PDOException $e){
				echo 'Error: '.$e->getMessage();
			}
		}
		function encerrarInscricao($id)
		{
			$sql = "SELECT `Situacao` FROM `tbtorneio` WHERE `IDTorneio`=".$id;
			echo "$sql";
		}
		function deletarInscricao($id){
			try{
				$sql = "DELETE FROM `tbinscricao` WHERE IDTorneio=".$id;
				$stm = $this->con->prepare($sql);
				$stm->execute();
				$this->deletarChaves($id);
			}catch(PDOException $e){
				echo 'Error: '.$e->getMessage();
			}
		}
		function deletarChaves($id){
			try{
				$sql = "DELETE FROM `tbgrupo` WHERE IDTorneio=".$id;
				$stm = $this->con->prepare($sql);
				$stm->execute();
			}catch(PDOException $e){
				echo 'Error: '.$e->getMessage();
			}
		}
		function listagemInscrito($id){
			try{
				$sql = "SELECT tbatleta.IDAtleta, tbatleta.NomeAtleta, YEAR(tbatleta.DataNasc), tbfaixa.NomeFaixa FROM tbinscricao INNER JOIN tbatleta INNER JOIN tbfaixa WHERE tbinscricao.IDAtleta=tbatleta.IDAtleta AND tbatleta.IDFaixa=tbfaixa.IDFaixa AND tbinscricao.IDTorneio=".$id;
				$stm = $this->con->prepare($sql);
				if ($stm->execute()){
					return $stm->fetchAll(PDO::FETCH_ASSOC);
				}else{
					return false;
				}
			}catch(PDOException $e){
				echo 'Error: '.$e->getMessage();
			}
		}
	}

?>