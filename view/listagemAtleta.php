<!DOCTYPE html>
<html>
<head>
	<title>Torneios</title>
</head>
<body>
	<?php
	include 'menu.php';
	include 'head.php';
	include 'verificaSessao.php';
	require_once 'mensagem.php';
	$m = new Mensagem();
	?>
	<div class="container ">
		<div class="row-md-4 g-3 text-center justify-content-center">
			<form action="../control/atletaControl.php" method="POST">
				<div class="col-4 form-group">
    				<label for="inputState">Ordenar por</label>
    				<select id="inputState" class="form-control" name="ordem">
		        		<option selected disabled="true" value="a">---</option>
 		       			<option value="nome">Nome</option>
						<option value="data">Ano de Nascimento</option>
						<option value="faixa">Faixa</option>
					</select>
					<br>
					<button type="submit" class="btn btn-lg btn-primary btn-block">Listar</button>
				</div>
				<input type="hidden" name="acao" value="2">
			</form>
			<?php
				if (isset($_SESSION['sensei'])):
			?>
			<div class="form-group col-md-4 ">
				<button onclick="location.href='formTorneio.php'" type="button" class="btn btn-success btn-lg btn-block">Novo Torneio</button>
			</div>
			<?php
				endif;
			?>
		</div>
		<div class="row-md-4 text-center justify-content-center">
			<?php if (isset($_SESSION['tornSucess'])):?>
				<div class="form-group col-md-4 ">
					<div class="alert alert-success"><?php $m->imprimeMensagem(9);?></div>
				</div>
			<?php unset($_SESSION['tornSucess']); endif;?>
			<?php if (isset($_SESSION['tornFail'])):?>
				<div class="form-group col-md-4 ">
					<div class="alert alert-danger"><?php $m->imprimeMensagem(10);?></div>
				</div>
			<?php unset($_SESSION['tornFail']);
			endif;?>
			<?php if (isset($_SESSION['delTornSucess'])):?>
				<div class="form-group col-md-4 ">
					<div class="alert alert-success"><?php $m->imprimeMensagem(12);?></div>
				</div>
			<?php unset($_SESSION['delTornSucess']); endif;?>
			<?php if (isset($_SESSION['delTornFail'])):?>
				<div class="form-group col-md-4 ">
					<div class="alert alert-danger"><?php $m->imprimeMensagem(13);?></div>
				</div>
			<?php unset($_SESSION['delTornFail']);
			endif;?>
			<?php if (isset($_SESSION['semCrit'])):?>
				<div class="form-group col-md-4 ">
					<div class="alert alert-danger"><?php $m->imprimeMensagem(11);?></div>
				</div>
			<?php unset($_SESSION['semCrit']);
			endif;?>
		</div>
	</div>

	<div>
		<?php
			if (isset($_SESSION['torneio'])) {
				$tab = ($_SESSION['torneio']);
				unset($_SESSION['torneio']);
				echo "<table class='table'> <thead> <tr> <td scope='col'>Nome</td> <td scope='col'>Data de Realização</td> <td scope='col'>Tipo</td> <td scope='col'>Valor de Inscrição</td> <td scope='col'>Situação de vagas</td> </tr> </thead> <tbody>";
				foreach ($tab as $chave) {
				echo "<tr>";
				echo "<td><a href='../view/detalheTorneio.php?id=".$chave['IDTorneio']."'>".$chave['NomeTorneio']."</a></td>";
				echo "<td>".$chave['DtTorneio']."</td>";
				echo "<td>".$chave['TipoTorneio']."</td>";
				echo "<td>".$chave['ValorInsc']."</td>";
				if ($chave['Situacao']==1) {
					echo "<td>Abertas</td>";
				} elseif ($chave['Situacao']==2) {
					echo "<td>Fechadas</td>";
				}
				
				//$x = 0;
				/*foreach ($chave as $key => $value) {
					if ($x == 1) {
						echo "<td><a href=''>".$value."</a></td>";
						$x++;
					} else{
						echo "<td>".$value."</td>";
						$x++;
					}
				}*/
				echo "</tr>";	
			}
			echo "</tbody>";
			}
		?>
	</div>
</body>
</html>