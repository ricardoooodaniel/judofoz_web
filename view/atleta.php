<!DOCTYPE html>
<html>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
<script type="text/javascript" src="../js/bootstrap.js"/>
<script type="text/javascript" src="../js/jquery.js" /></script>
<script type="text/javascript">
	function validarsenha() {
		var Senha = document.getElementById('senha');
		var Csenha = document.getElementById('csenha');
		if (Senha.value != Csenha.value) {
			Csenha.focus();
			document.getElementById('botao').disabled = true;
			alert('As senhas não coincidem');
			return false;
		} else{
			document.getElementById('botao').disabled = false;
		}
	}
</script>
<?php
	session_start();
	require_once 'mensagem.php';
	$m = new Mensagem();
?>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark" style="background-color: #002091">
		<a class="navbar-brand" href="../index.php">
		<img src="../img/Sistema/logo.jpeg" width="30" height="30" alt="">
		</a>
	</nav>
	<form action="../control/atletaControl.php" method="post" name="atleta" enctype="multipart/form-data">
		<div class="row">
		<div class="col-5">
			<label>Nome*</label>
			<input type="text" class="form-control" name="nome" name="" placeholder="Nome" required>
		</div>
		<div class="col-5">
			<label>Sobrenome*</label>
			<input type="text" class="form-control" name="sobrenome" placeholder="Sobrenome" required>
		</div>
		</div>
		<br>
		<div class="row">
		<div class="col-4">
			<label>Gênero*</label>
			<select name="genero" class="form-control" required>
				<option selected="selected" disabled="true" value="">Gênero</option>
				<option value="1">Masculino</option>
				<option value="2">Feminino</option>
			</select>
		</div>
		<div class="col-2">
			<label>Peso*</label>
			<input type="text" class="form-control" name="peso" placeholder="Peso" required>
		</div>
		<div class="col-2">
			<label>Altura*</label>
			<input type="text" class="form-control" name="altura" placeholder="Altura" required >
		</div>
		<div class="col-2">
			<label>Nascimento*</label>
			<input type="date" class="form-control" name="nascimento" required>
		</div>
		</div>
		<br>
		<div class="row">
		<div class="col-4">
			<label>Telefone</label>
			<input type="text" class="form-control" name="telefone" placeholder="Telefone">
		</div>
		<div class="col-5">
			<label>Email*</label>
			<input type="text" class="form-control" name="email" id="email" placeholder="Email" required>
		</div>
		</div>
		<br>
		<?php
		include 'endereco.php';
		?>
		<br>
		<div class="row">
		<div class="col-10">
			<label>Faixa*</label>
			<select name="faixa" class="form-control" required>
				<option selected="selected" disabled="true" value="">Faixa</option>
				<option value="1">Branca</option>
				<option value="2">Branca ponta Cinza</option>
				<option value="3">Cinza</option>
				<option value="4">Cinza ponta Azul</option>
				<option value="5">Azul</option>
				<option value="6">Azul ponta Amarela</option>
				<option value="7">Amarela</option>
				<option value="8">Amarela ponta Laranja</option>
				<option value="9">Laranja</option>
				<option value="10">Verde</option>
				<option value="11">Roxa</option>
				<option value="12">Marrom</option>
				<option value="13">Preta</option>
				<option value="14">Coral</option>
				<option value="15">Vermelha</option>
			</select>
		</div>
		</div>
		<br>
		<div class="row">
		<div class="col-10">
			<label>Login*</label>
			<input type="text" class="form-control" name="login" placeholder="Login" required>
		</div>
		</div>
		<br>
		<div class="row">
		<div class="col-5">
			<label>Senha*</label>
			<input type="password" class="form-control" name="senha" id="senha" placeholder="Senha" required>
		</div>
		<div class="col-5">
			<label>Confirmar Senha*</label>
			<input type="password" class="form-control" name="csenha" id="csenha" placeholder="Confirmar Senha" required onblur="return validarsenha();">
		</div>
		</div>
		<br>
		<div class="row ">
		<div class="col-10 text-center justify-content-center">
			<input class="form-control-file" type="file" name="imagem"><br>
		</div>
		</div>
		<br>
		<div class="row">
		<div class="col-10">
			<?php
				if (isset($_SESSION['existente'])):?>
				<div class="alert alert-danger">
				<?php
					$m->imprimeMensagem(2);
					unset($_SESSION['existente']);
					echo "</div>";
				endif;
			?>
			<?php
				if (isset($_SESSION['formatoIncompativel'])):?>
				<div class="alert alert-danger">
				<?php
					$m->imprimeMensagem(6);
					unset($_SESSION['formatoIncompativel']);
					echo "</div>";
				endif;
			?>
			<?php
				if (isset($_SESSION['ersenha'])):?>
				<div class="alert alert-danger">
				<?php
					$m->imprimeMensagem(7);
					unset($_SESSION['ersenha']);
					echo "</div>";
				endif;
			?>
		</div>
		</div>
		<div class="row">
		<div class="col-10 text-center justify-content-center">
			<button type="submit" class="btn btn-primary btn-lg mb-2" disabled="true" id="botao">Enviar</button>
		</div>
		</div>
		<input type="hidden" name="acao" value="1">
		</form>
<?php
	include 'rodape.php';
	?>
</body>
</html>