<!DOCTYPE html>
<html>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
<script type="text/javascript" src="../js/bootstrap.js"/>
<script type="text/javascript" src="../js/jquery.js" /></script>
<form>
	<div class="row">
		<div class="col-7">
			<label>Nome</label>
			<input type="text" class="form-control" placeholder="Nome">
		</div>
		<div class="col-3">
			<label>Cor</label>
			<input type="text" class="form-control" placeholder="Cor">
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-10">
			<label>Descrição</label>
			<input type="text" class="form-control" placeholder="Descrição">
		</div>
	</div>
</form>
</html>