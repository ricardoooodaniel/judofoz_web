<!DOCTYPE html>
<html>
<head>
	<?php
	include 'verificaSessao.php';
	include 'head.php';
	require_once 'mensagem.php';
	$m = new Mensagem();
	?>
	<title><?php echo $_SESSION['usuario']['NomeAtleta']; ?></title>
	<script type="text/javascript">
	function validarsenha() {
		var Senha = document.getElementById('nsenha');
		var Csenha = document.getElementById('csenha');
		if (Senha.value != Csenha.value) {
			Csenha.focus();
			document.getElementById('botao').disabled = true;
			alert('As senhas não coincidem');
			return false;
		} else{
			document.getElementById('botao').disabled = false;
		}
	}
</script>
</head>
<body>
	<?php
	include 'menu.php';
	?>
	<form action="../control/atletaControl.php" method="post" name="atleta" enctype="multipart/form-data">
	<div class="row">
		<div class="col-12">
			<label>Nome Completo*</label>
			<input type="text" class="form-control" name="nome" value="<?php echo $_SESSION['usuario']['NomeAtleta']; ?>" placeholder="Nome" required>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-5">
			<label>Gênero*</label>
			<select name="genero" class="form-control" required>
				<option selected="selected" disabled="true" value="">Gênero</option>
				<option value="1">Masculino</option>
				<option value="2">Feminino</option>
			</select>
		</div>
		<div class="col-2">
			<label>Peso*</label>
			<input type="text" class="form-control" value="<?php echo $_SESSION['usuario']['Peso']; ?>" name="peso" placeholder="Peso" required>
		</div>
		<div class="col-2">
			<label>Altura*</label>
			<input type="text" class="form-control" name="altura" value="<?php echo $_SESSION['usuario']['Altura']; ?>" placeholder="Altura" required >
		</div>
		<div class="col-3">
			<label>Nascimento*</label>
			<input type="date" value="<?php echo $_SESSION['usuario']['DataNasc']; ?>" class="form-control" name="nascimento" required>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-6">
			<label>Telefone</label>
			<input type="text" class="form-control" name="telefone" value="<?php echo $_SESSION['usuario']['TelefoneAtleta']; ?>" placeholder="Telefone">
		</div>
		<div class="col-6">
			<label>Email*</label>
			<input type="text" class="form-control" name="email" value="<?php echo $_SESSION['usuario']['EmailAtleta']; ?>" id="email" placeholder="Email" required>
		</div>
	</div>
	<!--<br>
	<?php
	//include 'endereco.php';
	?>
	<br>
	<div class="row">-->
		<div class="col-12">
			<label>Faixa*</label>
			<select name="faixa" class="form-control" required>
				<option selected="selected" disabled="true" value="">Faixa</option>
				<option value="1">Branca</option>
				<option value="2">Branca ponta Cinza</option>
				<option value="3">Cinza</option>
				<option value="4">Cinza ponta Azul</option>
				<option value="5">Azul</option>
				<option value="6">Azul ponta Amarela</option>
				<option value="7">Amarela</option>
				<option value="8">Amarela ponta Laranja</option>
				<option value="9">Laranja</option>
				<option value="10">Verde</option>
				<option value="11">Roxa</option>
				<option value="12">Marrom</option>
				<option value="13">Preta</option>
				<option value="14">Coral</option>
				<option value="15">Vermelha</option>
			</select>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-4">
			<label>Senha Antiga*</label>
			<input type="password" class="form-control" name="asenha" placeholder="Senha" required>
		</div>
		<div class="col-4">
			<label>Nova Senha</label>
			<input type="password" class="form-control" name="nsenha" id="nsenha" placeholder="Confirmar Senha" required>
		</div>
		<div class="col-4">
			<label>Confirmar Nova Senha</label>
			<input type="password" class="form-control" name="csenha" id="csenha" placeholder="Confirmar Senha" required onblur="return validarsenha();">
		</div>
	</div>
	<br>
	<div class="row ">
		<div class="col-10 text-center justify-content-center">
			<input class="form-control-file" type="file" name="imagem"><br>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-10">
			<?php
				/*if (isset($_SESSION['existente'])):?>
				<div class="alert alert-danger">
				<?php
					$m->imprimeMensagem(2);
					unset($_SESSION['existente']);
					echo "</div>";
				endif;
			?>
			<?php
				if (isset($_SESSION['formatoIncompativel'])):?>
				<div class="alert alert-danger">
				<?php
					$m->imprimeMensagem(6);
					unset($_SESSION['formatoIncompativel']);
					echo "</div>";
				endif;*/
			?>
			<?php
				if (isset($_SESSION['incsenha'])):?>
				<div class="alert alert-danger">
				<?php
					$m->imprimeMensagem(8);
					unset($_SESSION['incsenha']);
					echo "</div>";
				endif;
			?>
		</div>
	</div>
	<div class="row">
		<div class="col-6 text-right justify-content-right">
			<button type="submit" class="btn btn-primary btn-lg mb-2" id="botao">Salvar Alterações</button>
		</div>
		<div class="col-6 text-left justify-content-left">
			<button type="button" onclick="location.href='perfil.php'" class="btn btn-danger btn-lg mb-2">Voltar</button>
		</div>
	</div>
	<input type="hidden" name="acao" value="2">
	<input type="hidden" name="id" value="<?php echo $_SESSION['usuario']['IDAtleta']; ?>">
</form>
	<?php
	include 'rodape.php';
	?>
</body>
</html>