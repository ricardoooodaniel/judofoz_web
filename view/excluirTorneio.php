<!DOCTYPE html>
<html>
<head>
	<title>Excluir Torneio</title>
	<?php
	include 'head.php';
	include 'menu.php';
	include 'verificaSessao.php';
	require_once '../dao/torneioDAO.php';
	require_once 'mensagem.php';
	$m = new Mensagem();
	$dao = new TorneioDAO;
	$tab = $dao->listagemID($_GET['id']);
	//var_dump($tab);
	/*function Inscricao(){
		echo "<button type='submit' onclick='".realizaInsc(".$_SESSION['usuario']['IDAtleta'].")."'>Inscrever-se</button>";
	}
	function realizaInsc($id){

	}
	function butEncerrar(){
		echo "<button type='submit' onclick=''>Encerrar Inscrições</button>";
	}*/
	?>
</head>
<body>
	<div class="container">
		<div class="row align-itens-center text-center justify-content-center ">
			<div class="col-sm">
				<div class="h2">Tem certeza de que deseja excluir o torneio <?php echo $tab['NomeTorneio'];?>?</div>
			</div>
		</div>
	</div>
		<div class="container">
			<form action="../control/torneioControl.php" method="POST">
			<div class="row text-center justify-content-center">
			<div class="col-sm">
				<button type="submit" class="btn btn-lg btn-danger btn-block">Continuar</button>
			</div>
			<input type="hidden" name="acao" value="5">
			<input type="hidden" name="id" value="<?php echo $tab['IDTorneio']; ?>">
			<div class="col-sm">
				<button type="button" onclick="location.href='detalheTorneio.php?id=<?php echo $tab['IDTorneio'] ?>'" class="btn btn-lg btn-primary btn-block">Voltar</button>
			</div>
		</div>
		</form>
	</div>

</body>
</html>