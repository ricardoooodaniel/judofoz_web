<!DOCTYPE html>
<html>
<head>
	<title>Participantes</title>
</head>
<body>
	<?php
		include 'head.php';
		include 'menu.php';
		include 'verificaSessao.php';
		require_once '../dao/TorneioAtletaDAO.php';
		require_once '../dao/TorneioDAO.php';
		require_once 'mensagem.php';
		$m = new Mensagem();
		$dao = new TorneioAtletaDAO;
		$insc = $dao->listagemInscrito($_GET['id']);
		$d = new TorneioDAO;
		$tab = $d->listagemID($_GET['id']);
		//var_dump($tab);
		echo "<div class='h2 text-center justify-content-center'>".$tab['NomeTorneio']."</div>";
		echo "<table class='table'> <thead> <tr> <td scope='col'>Nome</td> <td scope='col'>Ano de nascimento</td> <td scope='col'>Faixa</td></tr> </thead> <tbody>";
				foreach ($insc as $chave) {
				echo "<tr>";
				echo "<td><a href='../view/detalheAtleta.php?id=".$chave['IDAtleta']."'>".$chave['NomeAtleta']."</a></td>";
				echo "<td>".$chave['YEAR(tbatleta.DataNasc)']."</td>";
				echo "<td>".$chave['NomeFaixa']."</td>";
				echo "</tr>";
				}
	?>
</body>
</html>
