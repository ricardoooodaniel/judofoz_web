<!DOCTYPE html>
<html>
<head>
	<?php
	require_once 'mensagem.php';
	$m = new Mensagem();
	include 'head.php';
	require_once '../dao/torneioDAO.php';
	$dao = new TorneioDAO();
	$torn = $dao->listagemID($_GET['id']);
	session_start();
	if (!isset($_SESSION['sensei'])) {
		header('Location: principal.php');
	}
	?>
	<meta charset="utf-8">
	<title><?php echo $torn['NomeTorneio'];?></title>
</head>
<body>
	<?php
	include 'menu.php';
	?>
	<form action="../control/torneioControl.php" method="POST">
		<div class="row">
			<div class="col-5">
				<label>Nome:</label>
				<input class="form-control" type="text" name="nome" value="<?php echo $torn['NomeTorneio']; ?>">
			</div>
			<div class="col-5">
				<label>Descrição:</label>
				<input class="form-control" type="text" name="descricao" value="<?php echo $torn['Descricao']; ?>">
			</div>
		</div>
		<div class="row">
			<div class="col-3">
				<label>Data de Realização:</label>
				<input class="form-control" type="date" name="DtTorneio" >
			</div>
			<div class="col-3">
				<label>Tipo de Pagamento:</label>
				<select class="form-control" name="pagamento">
					<option value="null" disabled="true" selected>---</option>
					<option value="nenhum">Nenhum</option>
					<option value="dinheiro">Monetário</option>
					<!--<option value="comida">Alimentício</option>-->
				</select><br>
			</div>
			<div class="col-4">
				<label>Valor:</label>
				<input class="form-control" type="text" name="valor" value="<?php echo $torn['ValorInsc']; ?>">
			</div>
		</div>
		<!--Tipo de Torneio:<select name="tptorneio">
			<option value="null" disabled="true">---</option>
			<option value="">1</option>
			<option value="">2</option>
			<option value="">3</option>
		</select><br>-->
		<br>
		<div class="row">
			<div class="col-10 text-center justify-content-center">
				<button type="submit" class="btn btn-success btn-lg mb-2">Concluir</button>
				<button type="button" class="btn btn-primary btn-lg mb-2">Voltar</button>
			</div>
		</div>
		<div class="row">
			<div class="col-10 text-center justify-content-center">
				<?php
				if (isset($_SESSION['torneioCamp'])):?>
				<div class="alert alert-danger">
				<?php
					$m->imprimeMensagem(17);
					unset($_SESSION['torneioCamp']);
					echo "</div>";
				endif;
			?>
			</div>
		</div>
		<input type="hidden" name="acao" value="6">
		<input type="hidden" name="id" value="<?php echo $torn['IDTorneio'];?>">
	</form>
	<?php
	include 'rodape.php';
	?>
</body>
</html>