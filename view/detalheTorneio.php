<!DOCTYPE html>
<html>
<head>
	<?php
	include 'head.php';
	include 'menu.php';
	include 'verificaSessao.php';
	require_once '../dao/torneioDAO.php';
	require_once 'mensagem.php';
	$m = new Mensagem();
	$dao = new TorneioDAO;
	$tab = $dao->listagemID($_GET['id']);
	//var_dump($tab);
	/*function Inscricao(){
		echo "<button type='submit' onclick='".realizaInsc(".$_SESSION['usuario']['IDAtleta'].")."'>Inscrever-se</button>";
	}
	function realizaInsc($id){

	}
	function butEncerrar(){
		echo "<button type='submit' onclick=''>Encerrar Inscrições</button>";
	}*/
	?>
	<title><?php echo $tab['NomeTorneio']; ?></title>
	<meta charset="utf-8">
</head>
<body>
	<section>
		<form action="../control/torneioControl.php" method="post">
		<table class="table table-bordered">
			<tr>
				<td class="td tabelaa">
					<label><b>Nome do Torneio:</b><?php echo $tab['NomeTorneio']; ?></label>
				</td>
				<td class="td tabelab">
					<label><b>Data de Realização(Ano/Mês/Dia):</b><?php echo $tab['DtTorneio']; ?></label>
				</td>
			</tr>
			<tr>
				<td class="td">
					<label><b>Tipo de Competição:</b><?php echo $tab['TipoTorneio']; ?></label>
				</td>
				<td class="td">
					<label><b>Valor de Inscrição:</b><?php echo $tab['ValorInsc']; ?></label>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<label><b>Situação de Vagas:</b><?php if ($tab['Situacao']==1) {
						echo "Aberta para inscrição";
					} else{
						echo "Inscrições Encerradas";
					}
					?></label>
				</td>
			</tr>
			<?php
				if (isset($_SESSION['atleta']) and $tab['Situacao']==1):
			?>
			<tr>
				<td colspan="2" align="center">
					<?php
					$dataAn = date('Y',strtotime($_SESSION['usuario']['DataNasc']));
					$dataTorn = date('Y',strtotime($tab['DtTorneio']));
					$diff = $dataTorn-$dataAn;
					?>
					<b>Selecione a Categoria de sua Inscrição:</b><br>
					<label><select class="form-control" name="categoria">
						<?php
							switch (true) {
								case ($diff<=10):
									echo "<option value='1'> LIVRE </option>";
									break;
								case ($diff<13):
									if ($_SESSION['usuario']['GeneroAtleta']=1&&$_SESSION['usuario']['IDFaixa']>=5) {
										echo "<option value='2'> SUB13 </option>";
									}elseif ($_SESSION['usuario']['GeneroAtleta']=2&&$_SESSION['usuario']['IDFaixa']>=3) {
										echo "<option value='2'> SUB13 </option>";
									}
									break;
								case ($diff<15):
									if ($_SESSION['usuario']['GeneroAtleta']=1&&$_SESSION['usuario']['IDFaixa']>=7) {
										echo "<option value='3'> SUB15 </option>";
									}elseif ($_SESSION['usuario']['GeneroAtleta']=2&&$_SESSION['usuario']['IDFaixa']>=5) {
										echo "<option value='3'> SUB15 </option>";
									}
									break;
								case ($diff<18):
									if ($_SESSION['usuario']['GeneroAtleta']=1&&$_SESSION['usuario']['IDFaixa']>=10) {
										echo "<option value='4'> SUB18 </option>";
									}elseif ($_SESSION['usuario']['GeneroAtleta']=2&&$_SESSION['usuario']['IDFaixa']>=7) {
										echo "<option value='4'> SUB18 </option>";
									}
									if ($_SESSION['usuario']['GeneroAtleta']=1&&$_SESSION['usuario']['IDFaixa']>=11) {
										echo "<option value='5'> SUB21 </option>";
										echo "<option value='6'> SÊNIOR </option>";
									}elseif ($_SESSION['usuario']['GeneroAtleta']=2&&$_SESSION['usuario']['IDFaixa']>=9) {
										echo "<option value='5'> SUB21 </option>";
										echo "<option value='6'> SÊNIOR </option>";
									}
									break;
								case ($diff<21):
									if ($_SESSION['usuario']['GeneroAtleta']=1&&$_SESSION['usuario']['IDFaixa']>=11) {
										echo "<option value='5'> SUB21 </option>";
										echo "<option value='6'> SÊNIOR </option>";
									}elseif ($_SESSION['usuario']['GeneroAtleta']=2&&$_SESSION['usuario']['IDFaixa']>=9) {
										echo "<option value='5'> SUB21 </option>";
										echo "<option value='6'> SÊNIOR </option>";
									}
									break;
								case ($diff>=21):
									if ($_SESSION['usuario']['GeneroAtleta']=1&&$_SESSION['usuario']['IDFaixa']>=11) {
										echo "<option value='6'> SÊNIOR </option>";
									}elseif ($_SESSION['usuario']['GeneroAtleta']=2&&$_SESSION['usuario']['IDFaixa']>=9) {
										echo "<option value='6'> SÊNIOR </option>";
									}
									break;
								
								default:
									break;
							}
						?>
					</select></label>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<label><button type="submit" class="btn btn-lg btn-primary btn-block">Inscrever-se</button></label>
				</td>
			</tr>
			<input type="hidden" name="acao" value="3">
			<?php
				endif;
				if (isset($_SESSION['sensei']) and $tab['Situacao']==1):
			?>
			<tr>
				<td align="right">
					<label><button type="submit" class="btn btn-lg btn-danger btn-block">Encerrar Inscrições</button></label>
				</td>
			<input type="hidden" name="acao" value="4">
			<?php
					if (isset($_SESSION['sensei'])):
						?>
				<td align="left">
					<label><button type="button" onclick="location.href='excluirTorneio.php?id=<?php echo $tab['IDTorneio'] ?>'" class="btn btn-lg btn-danger btn-block">Excluir Torneio</button></label>
				</td>
			</tr>
			<tr>
				<td align="center" colspan="2">
					<label><button type="button" onclick="location.href='alterarTorneio.php?id=<?php echo $tab['IDTorneio'] ?>'" class="btn btn-lg btn-primary btn-block">Alterar Torneio</button></label>
				</td>
			</tr>
			<?php
					endif;
				elseif (isset($_SESSION['sensei'])):
			?>
			<tr>
				<td align="center" colspan="2">
					<label><button type="button" onclick="location.href='excluirTorneio.php?id=<?php echo $tab['IDTorneio'] ?>'" class="btn btn-lg btn-danger btn-block">Excluir Torneio</button></label>
				</td>
			</tr>
			<input type="hidden" name="IDTorneio" value="<?php echo $tab['IDTorneio']  ?>">
		<?php endif; ?>
			<tr>
				<td colspan="2" align="center">
					<label><button type="button" onclick="location.href='inscritos.php?id=<?php echo $_GET['id'];?>'" class="btn btn-lg btn-success btn-block">Listar Inscritos</button></label>
				</td>
			</tr>
			<input type="hidden" name="IDTorneio" value="<?php echo $tab['IDTorneio']  ?>">
			<tr>
				<td colspan="2" align="center">
			<?php
			if (isset($_SESSION['inscrito'])):?>
				<div class="alert alert-danger">
				<?php
				$m->imprimeMensagem(5);
				echo "</div>";
				unset($_SESSION['inscrito']);
			endif;
			?>
			<?php
			if (isset($_SESSION['gerado'])):?>
				<div class="alert alert-success">
				<?php
				$m->imprimeMensagem(0);
				echo "</div>";
				unset($_SESSION['gerado']);
			endif;
			?>
			<?php
			if (isset($_SESSION['gerfalhado'])):?>
				<div class="alert alert-danger">
				<?php
				$m->imprimeMensagem(1);
				echo "</div>";
				unset($_SESSION['gerfalhado']);
			endif;
			?>
			<?php
			if (isset($_SESSION['semCat'])):?>
				<div class="alert alert-danger">
				<?php
				$m->imprimeMensagem(14);
				echo "</div>";
				unset($_SESSION['semCat']);
			endif;
			?>
				</td>
			</tr>
		</table>
		</form>
	</section>
</body>
</html>