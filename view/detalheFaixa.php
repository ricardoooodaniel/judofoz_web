<!DOCTYPE html>
<html>
<head>
	<?php
	include 'head.php';
	include 'verificaSessao.php';
	?>
	<title><?php echo $_SESSION['usuario']['Faixa']['NomeFaixa']; ?></title>
	<meta charset="utf-8">
</head>
<body>
	<?php
		include 'menu.php';
	?>
	<div class="container">
		<div class="row text-center justify-content-center">
			<div class="col-sm ">
				<img width="250" height="auto" src="../img/Sistema/Faixas/<?php echo $_SESSION['usuario']['Faixa'][3]?>">
			</div>
		</div>
		<div class="row">
			<div class="col-sm">Nome Japonês: <?php echo $_SESSION['usuario']['Faixa']['NomeJapones'];?></div>
			<div class="col-sm">Significado: <?php echo $_SESSION['usuario']['Faixa']['Descricao'];?> </div>
			<div class="col-sm">Outra coisa: </div>
		</div>
	</div>
</body>
</html>