<!DOCTYPE html>
<html>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
<script type="text/javascript" src="../js/bootstrap.js"/>
<script type="text/javascript" src="../js/jquery.js" /></script>
<form>
	<div class="row">
		<div class="col-5">
			<label>Nome</label>
			<input type="text" class="form-control" placeholder="Nome">
		</div>
		<div class="col-5">
			<label>Sobrenome</label>
			<input type="text" class="form-control" placeholder="Sobrenome">
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-4">
			<label>Peso</label>
			<input type="text" class="form-control" placeholder="Peso">
		</div>
		<div class="col-4">
			<label>Altura</label>
			<input type="text" class="form-control" placeholder="Altura">
		</div>
		<div class="col-2">
			<label>Nascimento</label>
			<input type="date" class="form-control">
		</div>
	</div>
</form>
</html>