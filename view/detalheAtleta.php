<!DOCTYPE html>
<html>
<head>
	<?php
		require_once '../dao/AtletaDAO.php';
		require_once 'mensagem.php';
		$m = new Mensagem();
		$dao = new AtletaDAO;
		$tab = $dao->consulta($_GET['id']);
	?>
	<title><?php echo $tab['NomeAtleta']; ?></title>
</head>
<body>
	<?php
		include 'head.php';
		include 'menu.php';
		include 'verificaSessao.php';
		$dn = new DateTime($tab['DataNasc']);
		$at = new DateTime();
		$idade = $at->diff($dn);
	?>
	<div class="container">
		<table class="table">
			<tr>
				<div class="col-3">
				<td rowspan="3" align="center"><img class="rounded-circle img-perfil" width="200" height="auto" src="../img/Atletas/<?php echo $tab['FotoAtleta']; ?>" alt="Perfil" /></td>
				<td><label><b>Nome:</b><?php echo $tab['NomeAtleta']; ?></label></td>
				<td><label><b>Idade:</b><?php echo $idade->y; ?></label></td>
				<td><label><b>Gênero:</b>
					<?php if ($tab['GeneroAtleta']==1) {
						echo "Masculino";
					}elseif ($tab['GeneroAtleta']==2){
						echo "Feminino";
					}
					?>
				</label></td>
			</div>
			</tr>
			<tr>
				<td><label><b>Data de Nascimento: </b><?php echo $tab['DataNasc'];  ?></label></td>
				<td><label><b>Altura: </b><?php echo $tab['Altura']; ?></label></td>
				<td><label><b>Peso: </b><?php echo $tab['Peso']; ?></label></td>
			</tr>
			<tr>
				<td><label><b>Graduação: </b><?php echo $tab['Faixa']['NomeFaixa']; ?></label></td>
				<td><label><b>Academia: </b><?php echo "Judofoz"; ?></label></td>
				<td><label><b>CEP: </b><?php echo $tab['Endereco']['CEP']; ?></label></td>
			</tr>
			<tr>
				<?php
					
				?>
				<td align="center"><label><a href="detalheFaixa.php"><img class="img-faixa" width="100" height="auto" src="../img/Sistema/Faixas/<?php echo $tab['Faixa'][3]?>"></a></label></td>
				<td><label><b>Estado: </b><?php echo $tab['Endereco']['Estado']; ?></label></td>
				<td><label><b>Município: </b><?php echo $tab['Endereco']['Municipio']; ?></label></td>
				<td><label><b>Alguma coisa: </b></label></td>
			</tr>
			<tr>
				<td><label><b>Bairro: </b><?php echo $tab['Endereco']['Bairro']; ?></label></td>
				<td><label><b>Rua: </b><?php echo $tab['Endereco']['Rua']; ?></label></td>
				<td><label><b>Número: </b><?php echo $tab['Endereco']['NResidencia']; ?></label></td>
				<td><label><b>Descrição: </b><?php echo $tab['Endereco']['Complemento']; ?></label></td>
			</tr>
		</table>
	</div>
</body>
</html>