<!DOCTYPE html>
<html>
<head>
	<?php
	session_start();
	if (!isset($_SESSION['sensei'])) {
		header('Location: principal.php');
	}
	?>
	<meta charset="utf-8">
	<title>Cadastro de Torneio</title>
</head>
<body>
	<?php
	include 'menu.php';
	?>
	<form action="../control/torneioControl.php" method="POST">
		<div class="row">
			<div class="col-5">
				<label>Nome:</label>
				<input class="form-control" type="text" name="nome">
			</div>
			<div class="col-5">
				<label>Descrição:</label>
				<input class="form-control" type="text" name="descricao">
			</div>
		</div>
		<div class="row">
			<div class="col-3">
				<label>Data de Realização:</label>
				<input class="form-control" type="date" name="DtTorneio">
			</div>
			<div class="col-3">
				<label>Tipo de Pagamento:</label>
				<select class="form-control" name="pagamento">
					<option value="null" disabled="true" selected>---</option>
					<option value="nenhum">Nenhum</option>
					<option value="dinheiro">Monetário</option>
					<!--<option value="comida">Alimentício</option>-->
				</select><br>
			</div>
			<div class="col-4">
				<label>Valor:</label>
				<input class="form-control" type="text" name="valor">
			</div>
		</div>
		<!--Tipo de Torneio:<select name="tptorneio">
			<option value="null" disabled="true">---</option>
			<option value="">1</option>
			<option value="">2</option>
			<option value="">3</option>
		</select><br>-->
		<?php
		include 'endereco.php';
		?>
		<br>
		<div class="row">
			<div class="col-10 text-center justify-content-center">
				<button type="submit" class="btn btn-primary btn-lg mb-2">Enviar</button>
			</div>
		</div>
		<input type="hidden" name="acao" value="1">
	</form>
	<?php
	include 'rodape.php';
	?>
</body>
</html>