<!DOCTYPE html>
<html>
<head>
	<title>Perfil</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>
	<?php
	include 'verificaSessao.php';
	include 'menu.php';
	//var_dump($_SESSION['usuario']);
	$dn = new DateTime($_SESSION['usuario']['DataNasc']);
	$at = new DateTime();
	$idade = $at->diff($dn);

	//$tm = strtodate($_SESSION['usuario']['DataNasc']);
	//$br = date("d/m/Y",$tm);
	?>
	<div class="container">
		<table class="table">
			<tr>
				<div class="col-3">
				<td rowspan="3" align="center"><img class="rounded-circle img-perfil" width="200" height="auto" src="../img/Atletas/<?php echo $_SESSION['usuario']['FotoAtleta']; ?>" alt="Perfil" /></td>
				<td><label><b>Nome:</b><?php echo $_SESSION['usuario']['NomeAtleta']; ?></label></td>
				<td><label><b>Idade:</b><?php echo $idade->y; ?></label></td>
				<td><label><b>Gênero:</b>
					<?php if ($_SESSION['usuario']['GeneroAtleta']==1) {
						echo "Masculino";
					}elseif ($_SESSION['usuario']['GeneroAtleta']==2){
						echo "Feminino";
					}
					?>
				</label></td>
			</div>
			</tr>
			<tr>
				<td><label><b>Data de Nascimento: </b><?php echo $_SESSION['usuario']['DataNasc'];  ?></label></td>
				<td><label><b>Altura: </b><?php echo $_SESSION['usuario']['Altura']; ?></label></td>
				<td><label><b>Peso: </b><?php echo $_SESSION['usuario']['Peso']; ?></label></td>
			</tr>
			<tr>
				<td><label><b>Graduação: </b><?php echo $_SESSION['usuario']['Faixa']['NomeFaixa']; ?></label></td>
				<td><label><b>Academia: </b><?php echo "Judofoz"; ?></label></td>
				<td><label><b>CEP: </b><?php echo $_SESSION['usuario']['Endereco']['CEP']; ?></label></td>
			</tr>
			<tr>
				<?php
					
				?>
				<td align="center"><label><a href="detalheFaixa.php"><img class="img-faixa" width="100" height="auto" src="../img/Sistema/Faixas/<?php echo $_SESSION['usuario']['Faixa'][3]?>"></a></label></td>
				<td><label><b>Estado: </b><?php echo $_SESSION['usuario']['Endereco']['Estado']; ?></label></td>
				<td><label><b>Município: </b><?php echo $_SESSION['usuario']['Endereco']['Municipio']; ?></label></td>
				<td><label><b>Alguma coisa: </b></label></td>
			</tr>
			<tr>
				<td><label><b>Bairro: </b><?php echo $_SESSION['usuario']['Endereco']['Bairro']; ?></label></td>
				<td><label><b>Rua: </b><?php echo $_SESSION['usuario']['Endereco']['Rua']; ?></label></td>
				<td><label><b>Número: </b><?php echo $_SESSION['usuario']['Endereco']['NResidencia']; ?></label></td>
				<td><label><b>Descrição: </b><?php echo $_SESSION['usuario']['Endereco']['Complemento']; ?></label></td>
			</tr>
		</table>
	</div>
</body>
</html>