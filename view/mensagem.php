<?php
	
	class Mensagem
	{
		private $mensagem = array('Inscrição Realizada com Sucesso',
					  'Problemas com a Inscrição, tente novamente mais tarde',
					  'Login ou Email já cadastrado, tente outro',
					  'Login ou senha incorretos, tente novamente',
					  'Cadastro realizado com sucesso, faça login para continuar',
					  'Você já se inscreveu nesse torneio, tente outro disponível',
					  'Formato de imagem incompatível, tente outro',
					  'Senhas não coincidem, as senha precisam ser iguais',
					  'Senha incorreta, alterações não poderão ser feitas',
					  'Torneio criado com sucesso',
					  'Erro ao criar Torneio, tente novamente mais tarde',
					  'Escolha um critério para ordenar a listagem',
					  'Torneio deletado com sucesso',
					  'Erro ao tentar deletar, tente novamente mais tarde',
					  'Você não possui os requisitos necessários para participar de torneios',
					  'Torneio alterado com sucesso',
					  'Falha em alternar torneio, tente novamente mais tarde',
					  'Preencha todos os campos');

		public function imprimeMensagem($i){
			echo $this->mensagem[$i];
		}
	}
	/*
		0 - Inscrição bem sucedida
		1 - problema com inscrição
		2 - Cadastro já Existe
		3 - Login ou Senha errados
		4 - Cadastro feito com sucesso
		5 - Já está inscrito naquele torneio
		6 - Formato de Imagem incompatível
		7 - Senhas diferentes
		8 - Senha Antiga incorreta
		9 - Torneio criado
		10 - Erro ao criar Torneio
		11 - Sem critério para listagem
		12 - Torneio deletado
		13 - Erro ao deletar Torneio
		14 - Usuário não pode se inscrever em torneios
		15 - Torneio alterado
		16 - falha em alterar torneio
		17 - campos do formulário de alteração de torneio vazios
	*/
?>