<script type="text/javascript" src="../js/ajax.js"/>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
<script type="text/javascript" src="../js/bootstrap.js"/>
<script type="text/javascript" src="../js/jquery.js" /></script>
	<div class="row">
		<div class="col-10">
			<label>CEP*</label>
			<input type="text" class="form-control" name="cep" id="cep" placeholder="CEP" required value="99999999" onblur="buscaendereco(this)">
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-5">
			<label>Estado*</label>
			<input type="text" class="form-control" name="estado" id="estado" placeholder="Estado" required value="Yomotsu Hirasaka">
		</div>
		<div class="col-5">
			<label>Cidade*</label>
			<input type="text" class="form-control" name="cidade" placeholder="Cidade" required value="Roma">
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-4">
			<label>Bairro*</label>
			<input type="text" class="form-control" name="bairro" placeholder="Bairro" required value="Jardim do Pão de Alho">
		</div>
		<div class="col-5">
			<label>Rua*</label>
			<input type="text" class="form-control" name="rua" placeholder="Rua" required value="Rua das morças">
		</div>
		<div class="col-1">
			<label>Número*</label>
			<input type="number" class="form-control" name="numero" placeholder="número" required value="666">
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-10">
			<label>Complemento</label>
			<input type="text" class="form-control" name="complemento" placeholder="Complemento" value="A morada de sua mãe">
		</div>
	</div>