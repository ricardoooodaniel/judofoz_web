
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
    <script type="text/javascript" src="../js/bootstrap.js"/>
    <script src="../js/jquery.js"></script>

<nav class="navbar navbar-expand-lg navbar-dark" style="background-color: #002091">
  <a class="navbar-brand" href="../index.php">
  <img src="../img/Sistema/logo.jpeg" width="30" height="30" alt="">
  </a>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="principal.php">Home <span class="sr-only">(current)</span></a>
      </li>
      <!--<li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Dropdown
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>-->
      <li class="nav-item">
        <a class="nav-link" href="listagemTorn.php">Torneios</a>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#">Atletas</a>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#">Disabled</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="perfil.php">Meu Perfil</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../control/sair.php">Sair</a>
      </li>
    </ul>
  </div>
</nav>
<hr>