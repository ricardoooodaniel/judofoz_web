-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Tempo de geração: 08-Mar-2021 às 13:26
-- Versão do servidor: 5.7.31
-- versão do PHP: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `dbwebjudofoz`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbatleta`
--

DROP TABLE IF EXISTS `tbatleta`;
CREATE TABLE IF NOT EXISTS `tbatleta` (
  `IDAtleta` int(15) NOT NULL AUTO_INCREMENT,
  `Login` varchar(50) NOT NULL,
  `Senha` varchar(32) NOT NULL,
  `EmailAtleta` varchar(50) NOT NULL,
  `TelefoneAtleta` varchar(15) NOT NULL,
  `NomeAtleta` varchar(50) NOT NULL,
  `TipoAtleta` int(1) NOT NULL DEFAULT '1',
  `IDFaixa` int(5) NOT NULL,
  `GeneroAtleta` int(1) NOT NULL,
  `RegFederacao` varchar(30) NOT NULL DEFAULT 'pendente',
  `DataNasc` date NOT NULL,
  `Peso` float NOT NULL,
  `Altura` float NOT NULL,
  `IDEndereco` int(10) NOT NULL,
  `FotoAtleta` varchar(40) NOT NULL DEFAULT 'pendente.png',
  PRIMARY KEY (`IDAtleta`),
  UNIQUE KEY `Login` (`Login`),
  UNIQUE KEY `EmailAtleta` (`EmailAtleta`),
  KEY `IDFaixa` (`IDFaixa`),
  KEY `IDEndereco` (`IDEndereco`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbatleta`
--

INSERT INTO `tbatleta` (`IDAtleta`, `Login`, `Senha`, `EmailAtleta`, `TelefoneAtleta`, `NomeAtleta`, `TipoAtleta`, `IDFaixa`, `GeneroAtleta`, `RegFederacao`, `DataNasc`, `Peso`, `Altura`, `IDEndereco`, `FotoAtleta`) VALUES
(2, 'teste', '698dc19d489c4e4db73e28a713eab07b', 'ednaldoperera@gmail.com', '45970707070', 'Ednaldo Pereira', 2, 15, 1, 'pendente', '1985-02-19', 90.5, 1.75, 33, 'EalBoKOWoAUqOQ8.jpg'),
(9, 'Brustomito', '202cb962ac59075b964b07152d234b70', 'Brustomito@gmail.com', '45999999999', 'Matheus Brustolin', 1, 12, 2, 'pendente', '2020-11-30', 75.6, 1.8, 33, 'pendente'),
(10, 'ricardinxd', '202cb962ac59075b964b07152d234b70', 'Brustolindo@gmail.com', '45999999999', 'Matheus Brustolin', 1, 6, 1, 'pendente', '2021-02-18', 75.6, 1.8, 38, 'pendente'),
(11, 'Null', '202cb962ac59075b964b07152d234b70', 'Null@gmail.com', '45999999999', 'Matheus Brustolin', 1, 3, 1, 'pendente', '2016-06-15', 27.5, 1.8, 39, 'pendente.png'),
(12, 'a', '202cb962ac59075b964b07152d234b70', 'a@gmail.com', '45999999999', 'Matheus Brustolin', 1, 3, 1, 'pendente', '2013-07-18', 30, 1.8, 40, 'pendente.png'),
(13, 'Viper', '1364cfe200cfc7e4630bf46b531d78f4', 'viper@gmail.com', '45999999999', 'Sabine Pandemic', 1, 10, 2, 'pendente', '1995-08-09', 75.6, 1.73, 37, 'Viperfil.jpeg'),
(15, 'robin', '202cb962ac59075b964b07152d234b70', 'RobinHood@gmail.com', '45998013221', 'Robin Hood', 1, 10, 1, 'pendente', '1985-05-12', 70, 1.76, 43, '60326e7aba309.png'),
(17, 'kratos', '202cb962ac59075b964b07152d234b70', 'Kratos@gmail.com', '45998013221', 'Kratos Guerra', 1, 15, 1, 'pendente', '1972-07-12', 84, 1.76, 45, 'pendente.png'),
(18, 'thanos', '202cb962ac59075b964b07152d234b70', 'Thanos@gmail.com', '45998013221', 'Thanos InevitÃ¡vel', 1, 4, 1, 'pendente', '1998-09-07', 84, 2, 46, '60328771a9175.jpg'),
(19, 'login', '202cb962ac59075b964b07152d234b70', 'algumacoisa@gmail.com', '45998013221', 'Latinoware2019 Guerra', 1, 9, 1, 'pendente', '2021-02-04', 84, 1.76, 47, 'pendente.png'),
(23, 'aaaaaaaaaaa', '202cb962ac59075b964b07152d234b70', 'aaaaaaaaaaaaaaaa', '123', 'aaaaaaaaaaaaaaaaaaaa aaaaaaaaaaaaaaa', 1, 11, 1, 'pendente', '2021-02-18', 84, 1.76, 51, 'pendente.png'),
(30, 'natsu', '202cb962ac59075b964b07152d234b70', 'Natsudragneel@gmail.com', '45998013221', 'Natsu Dragneel', 1, 4, 1, 'pendente', '2010-01-15', 42.5, 1.45, 58, '60353e0f03c2f.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbcategoria`
--

DROP TABLE IF EXISTS `tbcategoria`;
CREATE TABLE IF NOT EXISTS `tbcategoria` (
  `IDCategoria` int(11) NOT NULL AUTO_INCREMENT,
  `NomeCategoria` varchar(255) NOT NULL,
  `IdadeMin` int(11) NOT NULL,
  `IdadeMax` int(11) NOT NULL,
  PRIMARY KEY (`IDCategoria`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbcategoria`
--

INSERT INTO `tbcategoria` (`IDCategoria`, `NomeCategoria`, `IdadeMin`, `IdadeMax`) VALUES
(1, 'LIVRE', 0, 10),
(2, 'SUB13', 11, 12),
(3, 'SUB15', 13, 14),
(4, 'SUB18', 15, 17),
(5, 'SUB21', 15, 20),
(6, 'SENIOR', 15, 100);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbclasse`
--

DROP TABLE IF EXISTS `tbclasse`;
CREATE TABLE IF NOT EXISTS `tbclasse` (
  `IDClasse` int(11) NOT NULL AUTO_INCREMENT,
  `NomeClasse` varchar(255) NOT NULL,
  PRIMARY KEY (`IDClasse`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbclasse`
--

INSERT INTO `tbclasse` (`IDClasse`, `NomeClasse`) VALUES
(1, 'S. LIGEIRO'),
(2, 'LIGEIRO'),
(3, 'MEIO LEVE'),
(4, 'LEVE'),
(5, 'MEIO MEDIO'),
(6, 'MEDIO'),
(7, 'MEIO PESADO'),
(8, 'PESADO');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbclassecategoria`
--

DROP TABLE IF EXISTS `tbclassecategoria`;
CREATE TABLE IF NOT EXISTS `tbclassecategoria` (
  `IDClasseCategoria` int(11) NOT NULL AUTO_INCREMENT,
  `IDClasse` int(11) NOT NULL,
  `IDCategoria` int(11) NOT NULL,
  `PesoMin` float NOT NULL,
  `PesoMax` float NOT NULL,
  `Genero` varchar(255) NOT NULL,
  PRIMARY KEY (`IDClasseCategoria`),
  KEY `FKClasse` (`IDClasse`),
  KEY `FKCategoria` (`IDCategoria`)
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbclassecategoria`
--

INSERT INTO `tbclassecategoria` (`IDClasseCategoria`, `IDClasse`, `IDCategoria`, `PesoMin`, `PesoMax`, `Genero`) VALUES
(1, 1, 1, 0, 0, '1'),
(2, 1, 1, 0, 0, '2'),
(3, 1, 2, 0, 28, '1'),
(4, 1, 2, 0, 28, '2'),
(5, 1, 3, 0, 36, '1'),
(6, 1, 3, 0, 36, '2'),
(7, 1, 4, 0, 50, '1'),
(8, 1, 4, 0, 40, '2'),
(9, 1, 5, 0, 55, '1'),
(10, 1, 5, 0, 44, '2'),
(11, 1, 6, 0, 55, '1'),
(12, 1, 6, 0, 44, '2'),
(15, 2, 1, 0, 0, '1'),
(16, 2, 1, 0, 0, '2'),
(17, 2, 2, 28, 31, '1'),
(18, 2, 2, 28, 31, '2'),
(19, 2, 3, 36, 40, '1'),
(20, 2, 3, 36, 40, '2'),
(21, 2, 4, 50, 55, '1'),
(22, 2, 4, 40, 44, '2'),
(23, 2, 5, 55, 60, '1'),
(24, 2, 5, 44, 48, '2'),
(25, 2, 6, 55, 60, '1'),
(26, 2, 6, 44, 48, '2'),
(29, 3, 1, 0, 0, '1'),
(30, 3, 1, 0, 0, '2'),
(31, 3, 2, 31, 34, '1'),
(32, 3, 2, 31, 34, '2'),
(33, 3, 3, 40, 44, '1'),
(34, 3, 3, 40, 44, '2'),
(35, 3, 4, 55, 60, '1'),
(36, 3, 4, 44, 48, '2'),
(37, 3, 5, 60, 66, '1'),
(38, 3, 5, 48, 52, '2'),
(39, 3, 6, 60, 66, '1'),
(40, 3, 6, 48, 52, '2'),
(43, 4, 1, 0, 0, '1'),
(44, 4, 1, 0, 0, '2'),
(45, 4, 2, 34, 38, '1'),
(46, 4, 2, 34, 38, '2'),
(47, 4, 3, 44, 48, '1'),
(48, 4, 3, 44, 48, '2'),
(49, 4, 4, 60, 66, '1'),
(50, 4, 4, 48, 52, '2'),
(51, 4, 5, 66, 73, '1'),
(52, 4, 5, 52, 57, '2'),
(53, 4, 6, 66, 73, '1'),
(54, 4, 6, 52, 57, '2'),
(57, 5, 1, 0, 0, '1'),
(58, 5, 1, 0, 0, '2'),
(59, 5, 2, 38, 42, '1'),
(60, 5, 2, 38, 42, '2'),
(61, 5, 3, 48, 53, '1'),
(62, 5, 3, 48, 53, '2'),
(63, 5, 4, 66, 73, '1'),
(64, 5, 4, 52, 57, '2'),
(65, 5, 5, 73, 81, '1'),
(66, 5, 5, 57, 63, '2'),
(67, 5, 6, 73, 81, '1'),
(68, 5, 6, 57, 63, '2'),
(71, 6, 1, 0, 0, '1'),
(72, 6, 1, 0, 0, '2'),
(73, 6, 2, 42, 47, '1'),
(74, 6, 2, 42, 47, '2'),
(75, 6, 3, 53, 58, '1'),
(76, 6, 3, 53, 58, '2'),
(77, 6, 4, 73, 81, '1'),
(78, 6, 4, 57, 63, '2'),
(79, 6, 5, 81, 90, '1'),
(80, 6, 5, 63, 70, '2'),
(81, 6, 6, 81, 90, '1'),
(82, 6, 6, 63, 70, '2'),
(85, 7, 1, 0, 0, '1'),
(86, 7, 1, 0, 0, '2'),
(87, 7, 2, 47, 52, '1'),
(88, 7, 2, 47, 52, '2'),
(89, 7, 3, 58, 64, '1'),
(90, 7, 3, 58, 64, '2'),
(91, 7, 4, 81, 90, '1'),
(92, 7, 4, 63, 70, '2'),
(93, 7, 5, 90, 100, '1'),
(94, 7, 5, 70, 78, '2'),
(95, 7, 6, 90, 100, '1'),
(96, 7, 6, 70, 78, '2'),
(99, 8, 1, 0, 0, '1'),
(100, 8, 1, 0, 0, '2'),
(101, 8, 2, 52, 60, '1'),
(102, 8, 2, 52, 60, '2'),
(103, 8, 3, 64, 73, '1'),
(104, 8, 3, 64, 73, '2'),
(105, 8, 4, 90, 255, '1'),
(106, 8, 4, 70, 255, '2'),
(107, 8, 5, 100, 255, '1'),
(108, 8, 5, 78, 255, '2'),
(109, 8, 6, 100, 255, '1'),
(110, 8, 6, 78, 255, '2');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbendereco`
--

DROP TABLE IF EXISTS `tbendereco`;
CREATE TABLE IF NOT EXISTS `tbendereco` (
  `IDEndereco` int(10) NOT NULL AUTO_INCREMENT,
  `CEP` int(8) NOT NULL,
  `Estado` text NOT NULL,
  `Municipio` text NOT NULL,
  `Bairro` text NOT NULL,
  `Rua` text NOT NULL,
  `NResidencia` varchar(10) NOT NULL,
  `Complemento` varchar(50) NOT NULL,
  PRIMARY KEY (`IDEndereco`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbendereco`
--

INSERT INTO `tbendereco` (`IDEndereco`, `CEP`, `Estado`, `Municipio`, `Bairro`, `Rua`, `NResidencia`, `Complemento`) VALUES
(33, 99999999, 'Yomotsu Hirasaka', 'Roma', 'Jardim do PÃ£o de Alho', 'Rua das morÃ§as', '666', 'A morada de sua mÃ£e'),
(34, 99999999, 'Yomotsu Hirasaka', 'Roma', 'Jardim do PÃ£o de Alho', 'Rua das morÃ§as', '666', 'A morada de sua mÃ£e'),
(35, 99999999, 'Yomotsu Hirasaka', 'Roma', 'Jardim do PÃ£o de Alho', 'Rua das morÃ§as', '666', 'A morada de sua mÃ£e'),
(36, 12345678, 'Acre', 'Kanto', 'Jardim do PÃ£o de queijo', 'Rua do Ibama', '579', 'Eu escolho vocÃª!'),
(37, 56789123, 'LÃ­quido', 'Split', 'Icebox', 'Bomb A', '157', 'Kaboom'),
(38, 99999999, 'Yomotsu Hirasaka', 'Roma', 'Jardim do PÃ£o de Alho', 'Rua das morÃ§as', '666', 'A morada de sua mÃ£e'),
(39, 99999999, 'Yomotsu Hirasaka', 'Roma', 'Jardim do PÃ£o de Alho', 'Rua das morÃ§as', '666', 'A morada de sua mÃ£e'),
(40, 99999999, 'Yomotsu Hirasaka', 'Roma', 'Jardim do PÃ£o de Alho', 'Rua das morÃ§as', '666', 'A morada de sua mÃ£e'),
(41, 99999999, 'Yomotsu Hirasaka', 'Roma', 'Jardim do PÃ£o de Alho', 'Rua das morÃ§as', '666', 'A morada de sua mÃ£e'),
(42, 99999999, 'Yomotsu Hirasaka', 'Roma', 'Jardim do PÃ£o de Alho', 'Rua das morÃ§as', '666', 'A morada de sua mÃ£e'),
(43, 99999999, 'Yomotsu', 'Roma', 'Jardim do PÃ£o de Alho', 'Rua das morÃ§as', '666', 'A morada de sua mÃ£e'),
(44, 99999999, 'Yomotsu Hirasaka', 'Roma', 'Jardim do PÃ£o de Alho', 'Rua das morÃ§as', '666', 'A morada de sua mÃ£e'),
(45, 99999999, 'Yomotsu Hirasaka', 'Roma', 'Jardim do PÃ£o de Alho', 'Rua das morÃ§as', '666', 'A morada de sua mÃ£e'),
(46, 99999999, 'Yomotsu Hirasaka', 'Roma', 'Jardim do PÃ£o de Alho', 'Rua das morÃ§as', '666', 'A morada de sua mÃ£e'),
(47, 99999999, 'Yomotsu Hirasaka', 'Roma', 'Jardim do PÃ£o de Alho', 'Rua das morÃ§as', '666', 'A morada de sua mÃ£e'),
(48, 99999999, 'Yomotsu Hirasaka', 'Roma', 'Jardim do PÃ£o de Alho', 'Rua das morÃ§as', '666', 'A morada de sua mÃ£e'),
(49, 99999999, 'Yomotsu Hirasaka', 'Roma', 'Jardim do PÃ£o de Alho', 'Rua das morÃ§as', '666', 'A morada de sua mÃ£e'),
(50, 99999999, 'Yomotsu Hirasaka', 'Roma', 'Jardim do PÃ£o de Alho', 'Rua das morÃ§as', '666', 'A morada de sua mÃ£e'),
(51, 99999999, 'Yomotsu Hirasaka', 'Roma', 'Jardim do PÃ£o de Alho', 'Rua das morÃ§as', '666', 'A morada de sua mÃ£e'),
(52, 99999999, 'Yomotsu Hirasaka', 'Roma', 'Jardim do PÃ£o de Alho', 'Rua das morÃ§as', '666', 'A morada de sua mÃ£e'),
(53, 99999999, 'este Ã© o cep:99999999', 'Roma', 'Jardim do PÃ£o de Alho', 'Rua das morÃ§as', '666', 'A morada de sua mÃ£e'),
(54, 99999999, 'este Ã© o cep:99999999', 'Roma', 'Jardim do PÃ£o de Alho', 'Rua das morÃ§as', '666', 'A morada de sua mÃ£e'),
(55, 99999999, 'este Ã© o cep:99999999', 'Roma', 'Jardim do PÃ£o de Alho', 'Rua das morÃ§as', '666', 'A morada de sua mÃ£e'),
(56, 99999999, 'este Ã© o cep:99999999', 'Roma', 'Jardim do PÃ£o de Alho', 'Rua das morÃ§as', '666', 'A morada de sua mÃ£e'),
(57, 99999999, 'este Ã© o cep:99999999', 'Roma', 'Jardim do PÃ£o de Alho', 'Rua das morÃ§as', '666', 'A morada de sua mÃ£e'),
(58, 99999999, 'este Ã© o cep:99999999', 'Roma', 'Jardim do PÃ£o de Alho', 'Rua das morÃ§as', '666', 'A morada de sua mÃ£e'),
(59, 99999999, 'Yomotsu Hirasaka', 'Roma', 'Jardim do PÃ£o de Alho', 'Rua das morÃ§as', '666', 'A morada de sua mÃ£e'),
(60, 99999999, 'Yomotsu Hirasaka', 'Roma', 'Jardim do PÃ£o de Alho', 'Rua das morÃ§as', '666', 'A morada de sua mÃ£e'),
(61, 99999999, 'Yomotsu Hirasaka', 'Roma', 'Jardim do PÃ£o de Alho', 'Rua das morÃ§as', '666', 'A morada de sua mÃ£e'),
(62, 99999999, 'Yomotsu Hirasaka', 'Roma', 'Jardim do PÃ£o de Alho', 'Rua das morÃ§as', '666', 'A morada de sua mÃ£e'),
(63, 99999999, 'Yomotsu Hirasaka', 'Roma', 'Jardim do PÃ£o de Alho', 'Rua das morÃ§as', '666', 'A morada de sua mÃ£e'),
(64, 99999999, 'Yomotsu Hirasaka', 'Roma', 'Jardim do PÃ£o de Alho', 'Rua das morÃ§as', '666', 'A morada de sua mÃ£e'),
(65, 99999999, 'Yomotsu Hirasaka', 'Roma', 'Jardim do PÃ£o de Alho', 'Rua das morÃ§as', '666', 'A morada de sua mÃ£e');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbfaixa`
--

DROP TABLE IF EXISTS `tbfaixa`;
CREATE TABLE IF NOT EXISTS `tbfaixa` (
  `IDFaixa` int(5) NOT NULL AUTO_INCREMENT,
  `NomeFaixa` varchar(30) NOT NULL,
  `Descricao` text NOT NULL,
  `Imagem` varchar(20) NOT NULL,
  `NomeJapones` varchar(20) NOT NULL,
  PRIMARY KEY (`IDFaixa`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbfaixa`
--

INSERT INTO `tbfaixa` (`IDFaixa`, `NomeFaixa`, `Descricao`, `Imagem`, `NomeJapones`) VALUES
(1, 'branca', 'Essa é a cor do desprendimento e da pureza.', '01.png', 'Mukyu'),
(2, 'branca p/cinza', '', '02.png', ''),
(3, 'cinza', 'A faixa cinza simboliza uma pequena evolução técnica em comparação a faixa branca, essa graduação é dada apenas para praticantes até cerca de 15 anos, para a classe sênior essa graduação já não se faz presente.', '03.png', 'Shiti-Kyu '),
(4, 'cinza p/azul', '', '04.png', ''),
(5, 'azul', 'Significa o Céu, através do qual a planta cresce até tornar-se uma árvore frondosa, enquanto o treinamento do judô progride. O azul se faz intermediário entre o branco e o amarelo. A amadurecimento e a expectativa da evolução se faz presente, visto que o judoca continua dando andamento ao seu aprendizado. A continuidade dos treinamento fará com que a evolução técnica seja natural, assim como a cor azul se faz no dia a dia da natureza.', '05.png', 'Ro-Kyu'),
(6, 'azul p/amarela', '', '06.png', ''),
(7, 'amarela', 'Assim como um sol que desponta todos os dias, ela significa que é um iniciante ou um recém nascido, que com o tempo irá crescendo e fortalecendo-se, até chegar à maturidade que corresponde à faixa preta', '07.png', 'Rokku Kyu '),
(8, 'amarela p/laranja', '', '08.png', ''),
(9, 'laranja', 'Esta é uma cor que é a mistura do vermelho com o amarelo, representado que o conhecimento do grau anterior deve estar contido nesta graduação e trazendo as qualidades dessas duas cores', '09.png', 'Yon Kyu'),
(10, 'verde', 'O verde é uma cor que representa Esperança e a Fé. É a cor mais harmoniosa e calmante de todas. Ela simboliza harmonia e o equilíbrio.', '10.png', 'San Kyu '),
(11, 'roxa', 'O roxo é uma mistura das cores azul e vermelho.Essa é a cor usada pelos sacerdotes católicos para refletir santidade e humildade. Ela gera sentimentos como respeito próprio, dignidade e auto-estima.', '11.png', 'Ni Kyu '),
(12, 'marrom', 'É a cor da solidificação. Representa a constância, a disciplina, a uniformidade adquirida e a observação das regras mantidas até aqui', '12.png', 'Ichi Kyu'),
(13, 'preta', 'É a junção de todas as cores. Enfim o corpo e a mente chegaram ao final de uma jornada e ao início de outra mais elevada. A faixa na cor preta, representa humildade, autocontrole, maturidade, serenidade, disciplina com responsabilidade, dignidade e conhecimento', '13.png', 'Sho Dan '),
(14, 'coral', 'A faixa coral é caracterizada pelas cores branca e vermelha. Sugere a transição entre o inicial e o nivel de habilidade mais elevado presente após o suposto final.', '14.png', 'Rokudan'),
(15, 'vermelha', 'A cor vermelha sugere motivação, atividade e vontade. Ela atrai vida nova e pontos de partida inéditos', '15.png', 'Kyodan');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbgrupo`
--

DROP TABLE IF EXISTS `tbgrupo`;
CREATE TABLE IF NOT EXISTS `tbgrupo` (
  `IDGrupo` int(11) NOT NULL AUTO_INCREMENT,
  `IDClasseCategoria` int(11) DEFAULT NULL,
  `IDTorneio` int(11) DEFAULT NULL,
  `NumVagas` int(11) NOT NULL DEFAULT '4',
  PRIMARY KEY (`IDGrupo`),
  KEY `fkTorneio` (`IDTorneio`),
  KEY `fkClasseCategoria` (`IDClasseCategoria`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbinscricao`
--

DROP TABLE IF EXISTS `tbinscricao`;
CREATE TABLE IF NOT EXISTS `tbinscricao` (
  `ID` int(20) NOT NULL AUTO_INCREMENT,
  `IDAtleta` int(15) NOT NULL,
  `IDTorneio` int(10) NOT NULL,
  `IDClasseCategoria` int(11) NOT NULL,
  `IDGrupo` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDAtleta` (`IDAtleta`),
  KEY `IDTorneio` (`IDTorneio`),
  KEY `fk_ClasseCategoria` (`IDClasseCategoria`),
  KEY `fkGrupo` (`IDGrupo`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbinscricao`
--

INSERT INTO `tbinscricao` (`ID`, `IDAtleta`, `IDTorneio`, `IDClasseCategoria`, `IDGrupo`) VALUES
(28, 13, 2, 96, NULL),
(29, 15, 2, 53, NULL),
(30, 18, 2, 81, NULL),
(31, 30, 2, 73, NULL),
(32, 9, 2, 1, NULL),
(33, 17, 2, 81, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbtorneio`
--

DROP TABLE IF EXISTS `tbtorneio`;
CREATE TABLE IF NOT EXISTS `tbtorneio` (
  `IDTorneio` int(10) NOT NULL AUTO_INCREMENT,
  `NomeTorneio` varchar(50) NOT NULL,
  `Descricao` text NOT NULL,
  `DtTorneio` date NOT NULL,
  `TipoTorneio` int(1) NOT NULL DEFAULT '0',
  `NumInscritos` int(10) NOT NULL DEFAULT '0',
  `ValorInsc` float DEFAULT NULL,
  `IDEndereco` int(10) NOT NULL,
  `Situacao` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`IDTorneio`),
  KEY `IDEndereco` (`IDEndereco`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbtorneio`
--

INSERT INTO `tbtorneio` (`IDTorneio`, `NomeTorneio`, `Descricao`, `DtTorneio`, `TipoTorneio`, `NumInscritos`, `ValorInsc`, `IDEndereco`, `Situacao`) VALUES
(1, 'Latinoware2019', 'Boa d+', '2021-02-06', 0, 0, 0, 35, 1),
(2, 'Bulbasauro', 'planta', '2021-04-21', 0, 0, 0, 36, 1),
(3, 'Venhenu', 'sÃ³ Vipers', '2021-03-11', 0, 0, 15, 37, 1);

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `tbatleta`
--
ALTER TABLE `tbatleta`
  ADD CONSTRAINT `tbatleta_ibfk_1` FOREIGN KEY (`IDFaixa`) REFERENCES `tbfaixa` (`IDFaixa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbatleta_ibfk_2` FOREIGN KEY (`IDEndereco`) REFERENCES `tbendereco` (`IDEndereco`);

--
-- Limitadores para a tabela `tbgrupo`
--
ALTER TABLE `tbgrupo`
  ADD CONSTRAINT `fkClasseCategoria` FOREIGN KEY (`IDClasseCategoria`) REFERENCES `tbclassecategoria` (`IDClasseCategoria`),
  ADD CONSTRAINT `fkTorneio` FOREIGN KEY (`IDTorneio`) REFERENCES `tbtorneio` (`IDTorneio`);

--
-- Limitadores para a tabela `tbinscricao`
--
ALTER TABLE `tbinscricao`
  ADD CONSTRAINT `fkGrupo` FOREIGN KEY (`IDGrupo`) REFERENCES `tbgrupo` (`IDGrupo`),
  ADD CONSTRAINT `fk_ClasseCategoria` FOREIGN KEY (`IDClasseCategoria`) REFERENCES `tbclassecategoria` (`IDClasseCategoria`),
  ADD CONSTRAINT `tbinscricao_ibfk_1` FOREIGN KEY (`IDAtleta`) REFERENCES `tbatleta` (`IDAtleta`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbinscricao_ibfk_2` FOREIGN KEY (`IDTorneio`) REFERENCES `tbtorneio` (`IDTorneio`);

--
-- Limitadores para a tabela `tbtorneio`
--
ALTER TABLE `tbtorneio`
  ADD CONSTRAINT `tbtorneio_ibfk_1` FOREIGN KEY (`IDEndereco`) REFERENCES `tbendereco` (`IDEndereco`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
