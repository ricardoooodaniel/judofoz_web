<!DOCTYPE html>
<html>
   <head>
      <title>WebJudofoz</title>
      <meta charset="utf-8">
      <?php
         error_reporting(E_ALL);
         ini_set("display_errors", 1);
         session_start();
         require 'view/mensagem.php';
         $m = new Mensagem();
         if (isset($_SESSION['usuario'])) {
         	header('Location: view/principal.php');
         }
         ?>
      <script type="text/javascript" src="js/jquery.js" /></script>
      <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
      <script type="text/javascript" src="js/bootstrap.min.js"/></script>
   </head>
   <body style="background-color: #002091">
        
         <div class="container col-6 " style="background-color: #f2f2f2">
            <div class="row text-center justify-content-center" >
               <div class="col-6">
      <form class="form-signin" action="control/loginControl.php" method="post">
      <img class="mb-4" src="img/sistema/logo2.png" alt="" width="250" height="125">
      <h1 class="h3 mb-3 font-weight-normal">Faça Login</h1>
      <label for="inputEmail" class="sr-only">Login </label>
      <input type="text" id="inputEmail" class="form-control" placeholder="Login" required autofocus name="login">
      <label for="inputPassword" class="sr-only">Senha </label>
      <input type="password" id="inputPassword" class="form-control" placeholder="Senha" required name="senha">
      <div class="checkbox mb-3">
      <label>
      <a href="view/atleta.php">Não é cadastrado?</a>
      </label>
      </div>
      <?php
            if (isset($_SESSION['invalido'])):
            ?>
         <div class="alert alert-danger"><?php $m->imprimeMensagem(3)?></div>
         <?php
            endif;
            unset($_SESSION['invalido']);
            ?>
            <?php
            if (isset($_SESSION['cadastrado'])):
            ?>
          <div class="alert alert-success"><?php $m->imprimeMensagem(4)?></div>
         <?php
            endif;
            unset($_SESSION['cadastrado']);
            ?>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
      <p class="mt-5 mb-3 text-muted">&copy; 2020-2020</p>
      </form>
      </div>
      </div>
      </div>
   </body>
</html>