<?php
	require_once '../model/endereco.php';
	class Torneio{
		private $nome;
		private $desc;// descrição
		private $realizacao;//data de realização
		private $pagamento;//o tipo de pagamento a ser feito
		private $vpag;//valor do pagamento
		private $endereco;//id do endereco cadastrado
		public function getNome(){
			return $this->nome;
		}

		public function setNome($nome){
			$this->nome = $nome;
		}

		public function getDesc(){
			return $this->desc;
		}

		public function setDesc($desc){
			$this->desc = $desc;
		}

		public function getRealizacao(){
			return $this->realizacao;
		}

		public function setRealizacao($realizacao){
			$this->realizacao = $realizacao;
		}

		public function getPagamento(){
			return $this->pagamento;
		}

		public function setPagamento($pagamento){
			$this->pagamento = $pagamento;
		}

		public function getVpag(){
			return $this->vpag;
		}

		public function setVpag($vpag){
			$this->vpag = $vpag;
		}

		public function getEndereco(){
			return $this->endereco;
		}

		public function setEndereco($endereco){
			$this->endereco = $endereco;
		}
	}
?>