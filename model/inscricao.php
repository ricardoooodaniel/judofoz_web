<?php

class Inscricao{
	private $IDAtleta;
	private $IDTorneio;
	private $classeCategoria;

	public function getIDAtleta(){
			return $this->IDAtleta;
		}

	public function setIDAtleta($IDAtleta){
			$this->IDAtleta = $IDAtleta;
		}

	public function getIDTorneio(){
			return $this->IDTorneio;
		}

	public function setIDTorneio($IDTorneio){
			$this->IDTorneio = $IDTorneio;
		}
	
	public function getClasseCategoria(){
		return $this->classeCategoria;
	}

	public function setClasseCategoria($classeCategoria){
		$this->classeCategoria = $classeCategoria;
	}
}

?>