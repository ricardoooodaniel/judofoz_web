<?php
	require_once ('../model/login.php');
	require_once ('../dao/loginDAO.php');
	if (empty($_POST['login']) || empty($_POST['senha'])) {
		header('Location: ../index.php');
	}
	class LoginControl{
		private $dao;
		function __construct(){
			$this->dao = new loginDAO();
			$this->logar();
		}
		function logar(){
			session_start();
			$login = new Login();
			$login->setUsuario($_POST['login']);
			$login->setSenha($_POST['senha']);
			$num = $this->dao->verificar($login);
			if ($num==1) {
				$_SESSION['atleta'] = true;
				header('Location: ../view/principal.php');
			}elseif ($num==2) {
				$_SESSION['sensei'] = true;
				header('Location: ../view/principal.php');
			}elseif ($num==0) {
				$_SESSION['invalido'] = true;
				header('Location: ../index.php');
			}
		}
	}
	new LoginControl();
?>