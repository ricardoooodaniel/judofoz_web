<?php
	require_once '../model/torneio.php';
	require_once '../model/inscricao.php';
	require_once '../dao/torneioDAO.php';
	require_once '../dao/TorneioAtletaDAO.php';
	session_start();
	class TorneioControl{
		private $acao;
		private $dao;
		function __construct(){
			$this->dao = new TorneioDAO();
			$this->acao=$_REQUEST["acao"];
			$this->verificaAcao();
		}
		function verificaAcao(){
			switch ($this->acao) {
				case 1:
					$this->controleInsercao();
					break;
				case 2:
					$this->controleListagem();//dao->listagem('NomeTorneio');
					break;
				case 3:
					$this->inscricao();
					break;
				case 4:
					$this->encerrar();
					break;
				case 5:
					$this->deletar();
					break;
				case 6:
					$this->alterar();
					break;
				default:
					# code...
					break;
			}
		}
		function alterar(){
			if (isset($_POST['pagamento'])&&isset($_POST['nome'])&&isset($_POST['descricao'])&&isset($_POST['DtTorneio'])&&isset($_POST['valor'])&&isset($_POST['id'])) {
				$torneio = new Torneio();
				$torneio->setNome($_POST['nome']);
				$torneio->setDesc($_POST['descricao']);
				$torneio->setRealizacao($_POST['DtTorneio']);
				$torneio->setPagamento($_POST['pagamento']);
				$torneio->setVpag($_POST['valor']);
				$id = $_POST['id'];
				$this->dao->alterar($torneio,$id);
			}else{
				$_SESSION['torneioCamp']=true;
				header('Location: ../view/alterarTorneio.php?id='.$_POST['id']);
			}
			
		}
		function deletar(){
			$dao = new TorneioDAO();
			$id = $_POST['id'];
			$dao->deletarTorneio($id);
		}
		function encerrar(){
			$dao = new TorneioAtletaDAO();
			$id = $_POST['IDTorneio'];
			$dao->encerrarInscricao($id);
		}

		function inscricao(){
			if (!isset($_POST['categoria'])){
				$_SESSION['semCat']=true;
				header("Location:../view/detalhetorneio.php?id=".$_POST['IDTorneio']);
			}else{
				$classe;
				$inscricao = new Inscricao();
				$inscricao->setIDAtleta($_SESSION['usuario']['IDAtleta']);
				$inscricao->setIDTorneio($_POST['IDTorneio']);
				if ($_POST['categoria']==1) {
					$inscricao->setClasseCategoria(1);
				}else{
					$classe=$this->dao->consultaClasse($_POST['categoria']);
					foreach ($classe as $chave) {
						if ($_SESSION['usuario']['GeneroAtleta']==$chave['Genero']) {
							if ($_SESSION['usuario']['Peso']>=$chave['PesoMin']&&$_SESSION['usuario']['Peso']<$chave['PesoMax']) {
								$inscricao->setClasseCategoria($chave['IDClasseCategoria']);
							}
						}
					}
				}
				/* Classes
				0 - Sem Classe (menores de 11 anos)
				1 - S. Ligeiro
				2 - Ligeiro
				3 - Meio Leve
				4 - Leve
				5 - Meio Médio
				6 - Médio
				7 - Meio Pesado
				8 - Pesado
				9 - S. Pesado
				*/
				//var_dump($inscricao);
				$dao = new TorneioAtletaDAO();
				$estado = $dao->consultarInscricao($inscricao);
				switch ($estado) {
					case 1:
						$_SESSION['inscrito'] = true;
						break;
					case 2:
						$_SESSION['gerado'] = true;
						break;
					case 3:
						$_SESSION['gerfalhado'] = true;
						break;
					
					default:
						# code...
						break;
				}
				header('Location: ../view/detalhetorneio.php?id='.$inscricao->getIDTorneio());
			}
		}
		function controleInsercao(){
			$torneio = new Torneio();
			$torneio->setNome($_POST['nome']);
			$e = new Endereco();
			$e->setCEP($_POST['cep']);
			$e->setEstado($_POST['estado']);
			$e->setNumero($_POST['numero']);
			$e->setRua($_POST['rua']);
			$e->setBairro($_POST['bairro']);
			$e->setCidade($_POST['cidade']);
			$e->setComplemento($_POST['complemento']);
			$torneio->setEndereco($e);
			$torneio->setDesc($_POST['descricao']);
			$torneio->setRealizacao($_POST['DtTorneio']);
			$torneio->setPagamento($_POST['pagamento']);
			$torneio->setVpag($_POST['valor']);
			$this->dao->inserir($torneio);
		}
		function controleListagem(){
			$ordem = $_POST['ordem'];
			$tab;
			switch ($ordem) {
				case 'nome':
					$tab = $this->dao->listagem('NomeTorneio');
					break;
				case 'data':
					$tab = $this->dao->listagem('DtTorneio');
					break;
				case 'valor':
					$tab = $this->dao->listagem('ValorInsc');
					break;
				case 'a':
					$tab=null;
					break;
				default:
					break;
			}
			/*echo "<table><tr><td>";
			echo "<table border='solid'> <tr> <td>Nome</td> <td>Data de Realização</td> <td>Tipo</td> <td>Valor de Inscrição</td> <td>Situação de vagas</td> </tr>";
			foreach ($tab as $chave) {
				echo "<tr>";
				echo "<td><a target='detalhes' href='../view/detalheTorneio.php?id=".$chave['IDTorneio']."'>".$chave['NomeTorneio']."</a></td>";
				echo "<td>".$chave['DtTorneio']."</td>";
				echo "<td>".$chave['TipoTorneio']."</td>";
				echo "<td>".$chave['ValorInsc']."</td>";
				echo "<td>".$chave['Situacao']."</td>";
				//$x = 0;
				/*foreach ($chave as $key => $value) {
					if ($x == 1) {
						echo "<td><a href=''>".$value."</a></td>";
						$x++;
					} else{
						echo "<td>".$value."</td>";
						$x++;
					}
				}
				echo "</tr>";
			}
			echo"</table>";
			echo "<td><iframe name='detalhes' height='600' width='700' ></iframe></td></tr></table>";*/
			if ($tab<>null) {
				$_SESSION['torneio'] = $tab;
			}else{
				$_SESSION['semCrit'] = true;
			}
			header('Location: ../view/listagemTorn.php');
			
		}
	}
	new TorneioControl();
?>