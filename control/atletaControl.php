<?php
	require_once ('../model/atleta.php');
	require_once ('../dao/atletaDAO.php');
	class AtletaControl	{
		private $acao;
		private $dao;
		function __construct(){
			$this->dao = new atletaDAO();
			$this->acao=$_REQUEST["acao"];
			$this->verificaAcao();
		}
		function verificaAcao(){
			switch ($this->acao) {
				case 1:
					$this->controleInsercao();
					break;
				case 2:
					$this->editaAtleta();
					break;
				case 3:
					$this->listaAtleta();
					break;
				
				default:
					# code...
					break;
			}
		}
		function editaAtleta(){
			if (md5($_POST['asenha'])==$_SESSION['Senha']) {
				$i=$this->upload();
				if ($i==false) {
					header('Location: ../view/editaPerfil.php');
				}else{ 
					$atleta=new Atleta();
					$atleta->setNome($_POST['nome']);
					$atleta->setFaixa($_POST['faixa']);
					$peso=str_replace(',', '.', $_POST['peso']);
					$peso=floatval($peso);
					$atleta->setPeso($peso);
					$altura=str_replace(',', '.', $_POST['altura']);
					$altura=floatval($altura);
					$atleta->setAltura($altura);
					$atleta->setNascimento($_POST['nascimento']);
					$atleta->setUsuario($_POST['login']);
					$atleta->setSenha($_POST['senha']);
					$atleta->setGenero($_POST['genero']);
					$atleta->setImg($i);
					$e = new Endereco();
					$e->setCEP($_POST['cep']);
					$e->setEstado($_POST['estado']);
					$e->setNumero($_POST['numero']);
					$e->setRua($_POST['rua']);
					$e->setBairro($_POST['bairro']);
					$e->setCidade($_POST['cidade']);
					$e->setComplemento($_POST['complemento']);
					$atleta->setEndereco($e);
					$atleta->setEmail($_POST['email']);
					$atleta->setTelefone($_POST['telefone']);
					var_dump($atleta);
					$this->dao->alterar($atleta);
				}
			}else{
				$_SESSION['incsenha']= true;
				header('Location: ../view/editaPerfil.php');
			}
		}
		function controleInsercao(){
			$i=$this->upload();
			if ($i==false) {
				header('Location: ../view/atleta.php');
			}else{
				if ($_POST['senha']==$_POST['csenha']) {
					$atleta=new Atleta();
					$atleta->setNome($_POST['nome']." ".$_POST['sobrenome']);
					$atleta->setFaixa($_POST['faixa']);
					$peso=str_replace(',', '.', $_POST['peso']);
					$peso=floatval($peso);
					$atleta->setPeso($peso);
					$altura=str_replace(',', '.', $_POST['altura']);
					$altura=floatval($altura);
					$atleta->setAltura($altura);
					$atleta->setNascimento($_POST['nascimento']);
					$atleta->setUsuario($_POST['login']);
					$atleta->setSenha($_POST['senha']);
					$atleta->setGenero($_POST['genero']);
					$atleta->setImg($i);
					$e = new Endereco();
					$e->setCEP($_POST['cep']);
					$e->setEstado($_POST['estado']);
					$e->setNumero($_POST['numero']);
					$e->setRua($_POST['rua']);
					$e->setBairro($_POST['bairro']);
					$e->setCidade($_POST['cidade']);
					$e->setComplemento($_POST['complemento']);
					$atleta->setEndereco($e);
					$atleta->setEmail($_POST['email']);
					$atleta->setTelefone($_POST['telefone']);
					var_dump($atleta);
					$this->dao->inserir($atleta);
				}else{
					$_SESSION['ersenha']= true;
					header("Location: ../view/atleta.php");
				}
			}
		}
		function upload(){
			if ($_FILES['imagem']<>null) {
				$formato = array('png','jpeg','jpg');
				$img = $_FILES['imagem'];
				$nome = $img['tmp_name'];
				$tipo = pathinfo($img['name'], PATHINFO_EXTENSION);
				if (in_array($tipo, $formato)) {
					$pasta = '../img/Atletas/';
					$novonome = uniqid().".$tipo";
					if (move_uploaded_file($nome, $pasta.$novonome)) {
						return $novonome;
					}
				} else {
					$_SESSION['formatoIncompativel']=true;
					return false;
				}
			} else{
				return null;
				//header('Location: index.php');
			}
		}
	}
	new AtletaControl();
?>