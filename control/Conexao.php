<?php
class Conexao
{
    private $con;
    static function conectar(){
        $con=false;
        try{
            $usuario="root";
            $senha="";
            $base="dbwebjudofoz";
            $con = new PDO("mysql:host=localhost;dbname=".$base, $usuario, $senha);
        }catch(PDOException $e){
            echo "Erro ao conectar com banco de dados MySQL<hr>".$e->getMessage();
        }
        return $con;
    }
}

